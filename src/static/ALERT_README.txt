-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

▞▀▖▐  ▐        ▐  ▗                                      ▌
▙▄▌▜▀ ▜▀ ▞▀▖▛▀▖▜▀ ▄ ▞▀▖▛▀▖   ▌ ▌▞▀▖▌ ▌ ▝▀▖▙▀▖▞▀▖ ▌ ▌▛▀▖▞▀▌▞▀▖▙▀▖
▌ ▌▐ ▖▐ ▖▛▀ ▌ ▌▐ ▖▐ ▌ ▌▌ ▌▗▖ ▚▄▌▌ ▌▌ ▌ ▞▀▌▌  ▛▀  ▌ ▌▌ ▌▌ ▌▛▀ ▌
▘ ▘ ▀  ▀ ▝▀▘▘ ▘ ▀ ▀▘▝▀ ▘ ▘▗▘ ▗▄▘▝▀ ▝▀▘ ▝▀▘▘  ▝▀▘ ▝▀▘▘ ▘▝▀▘▝▀▘▘
               ▐  ▐
▝▀▖▙▀▖▙▀▖▞▀▖▞▀▘▜▀ ▐
▞▀▌▌  ▌  ▛▀ ▝▀▖▐ ▖▝
▝▀▘▘  ▘  ▝▀▘▀▀  ▀ ▝

Hello, you are probably wondering why you are seeing this page instead of your
cool picture.
I updated gnu.cat, you have to update your scripts with some changes or you
can download the new script tool to use all new features at /file.

For example, before you had cool 3 character plus file extension URIs, now you
can have 5 characters with or withour extension as an anonymous user, or you
can make an account (https://gnu.cat/data) and keep having 3 characters with
or withour extension.

You can also define the selfdestruct dates, to check the new usages, check the
manual page for the "files" tool at https://gnu.cat/file

For any inquiries please mail me,
Jaume Fuster i Claris <capuno [at] gnu [dt] cat>

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEx1G4fJXfg6PKriyf5z1t6lgrHDYFAmDq7NgACgkQ5z1t6lgr
HDasSwwAqjIzZJsDTmdnxRHtlTZOLloROFRzCJpdVgutSi96QCrwLnPBjca7vyR5
cdrJldkioq7RVFY26z5+IcNOMmjolQJlWYO2Hxq63D5FniqSx+iiBMtZLxeEmPtL
T3Pd6LpEGCdfb0glJK+ZyiUjHZflmKlrouVNzontHYlBQchXE5ZbtuqiH/mn5Ii4
ZpkH7gSC1Wo1DhlCUc5YQp67Y3dw5ozAliT7q9LWzaTee0xrl+X1LwPoi8yn07zO
/WEqk9p80H1HbkxgXdl55dGI4kk4eSVi0cM88sxaX+Opw2gNE/yqrEqxcL5rLPHG
ruapqC7ifBPL445uaA7Pg4A//Vr411I20EYTnMI3zVETYZBcbAV9YLg9SvkFn47e
A6t5TnTUhbKa4IeE2yqijcIALiPYml+FpjR1aZL3hn1BjqZ8VekwhVyfW00D+42r
iPsQaenVqLuB4HIBFs5Yty3U4OcfZgW9DcimjOo9P6ZNxPUlUagAwVHP5qBxd53y
uCUuFBpe
=61S7
-----END PGP SIGNATURE-----

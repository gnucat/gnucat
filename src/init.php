<?php

require "lib/utils.php";
require "lib/database.php";


define("PROTOCOL", (isset($_SERVER['HTTPS']) ? "https" :  "http")."://");
define("CONFIG", parse_ini_file("config.ini", true));
$DB = new DB(CONFIG["database"]);

<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title>GNUCAT</title>

	<?php include "static/html/includes.html" ?>

	<style type="text/css">
		section#people > div#members {
			flex-wrap: wrap;
			display: flex;
		}
		section#people div.member {
			margin-top: 1rem;
			flex-grow: 1;
			width: 50%;
		}
		section#people div.member > div {
			border-left: 5px solid orange;
			overflow: hidden;
			margin: 0 1rem;
			padding: 1rem;
		}
		section#people div.member > div > img:first-child {
			border: 1px solid var(--color-dark);
			margin-right: 1rem;
			max-width: 10rem;
			float: left;
		}
	</style>

</head>
<body>

	<?php include "static/html/header.html" ?>

	<main>

		<section id="notebook">
			<div class="note">
				<blockquote>All language is but a poor translation.</blockquote> Franz Kafka
			</div>
		</section>

		<section id="contact">
			<h2>Contact</h2>
			<table>
				<thead>
					<tr>
						<th>Channel</th>
						<th>Address</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>IRC</td>
						<td>#gnucat</td>
						<td>On irc.libera.chat</td>
					</tr>
					<tr>
						<td>MAIL</td>
						<td>abuse@gnu.cat</td>
						<td>For abuse inquiries.</td>
					</tr>
					<tr>
						<td>MAIL</td>
						<td>privacy@gnu.cat</td>
						<td>For privacy inquiries.</td>
					</tr>
					<tr>
						<td>WEB</td>
						<td><a href="#message">#message</a></td>
						<td>Send the GNUCAT Project a message anonymously.</td>
					</tr>
				</tbody>
			</table>
		</section>

		<section id="people">
			<h2>The People</h2>
			<div id="members">
				<div class="member">
					<h4>Jaume Fuster i Claris</h4>
					<strong>Project Leader</strong>
					<div>
						<img src="/img/people/capuno.jpeg">
						<p>
							I don't even like computers, computers are for nerds, and I'm not a nerd.
							<br>
							I hope one day we will all be able to live without computers, until then, I will try my best to destroy every piece of technology I find.
						</p>
						<a href="https://capuno.cat/">WEBSITE</a>
						<a href="/static/gpg/capuno.key">GPG KEY</a>
						<a href="mailto:capuno@gnu.cat">EMAIL</a>
						<a href="mailto:c@capuno.cat">EMAIL</a>
					</div>
				</div>
				<div class="member">
					<h4>Ivan Diviroch</h4>
					<strong>Emotional Support</strong>
					<div>
						<img src="/img/people/ivan.jpeg">
						<p>
							Hello, I am Ivan, a professional model for underwear, I helped the GNUCAT project with emotional support.
							<br>
							I am almost done.
						</p>
						<progress value="66" max="100"></progress>
					</div>
				</div>
				<div class="member">
					<h4>Ivan Diviroch</h4>
					<strong>Emotional Support</strong>
					<div>
						<img src="/img/people/ivan2.jpeg">
						<p>
							Hello, I am Ivan, a professional model for underwear, I helped the GNUCAT project with emotional support.
							<br>
							I am almost done.
						</p>
						<progress value="75" max="100"></progress>
					</div>
				</div>
				<div class="member">
					<h4>Ivan Diviroch</h4>
					<strong>Emotional Support</strong>
					<div>
						<img src="/img/people/ivan3.jpeg">
						<p>
							Hello, I am Ivan, a professional model for underwear, I helped the GNUCAT project with emotional support.
							<br>
							I am almost done.
						</p>
						<progress value="92" max="100"></progress>
					</div>
				</div>
				<div class="member">
					<h4>Ivan Diviroch</h4>
					<strong>Emotional Support</strong>
					<div>
						<img src="/img/people/ivan4.jpeg">
						<p>
							Hello, I am Ivan, a professional model for underwear, I helped the GNUCAT project with emotional support.
							<br>
							I am almost done.
							<br>
							Run.
						</p>
						<progress value="99" max="100"></progress>
					</div>
				</div>
			</div>
			<p style="max-width: 30rem">
				If for some insane reason you want to be listed in this section, please contact anyone already listed and tell them why you should be listed with the image and text to put.
			</p>
		</section>
		
	</main>

	<?php include "static/html/footer.html" ?>

</body>
</html>
<?php

	require "init.php";

	// TEMPORAL START
	if ($_SERVER["REQUEST_METHOD"] === "POST") {
		die("https://gnu.cat/static/ALERT_README.txt");
	}
	// TEMPORAL END

?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title>GNUCAT</title>

	<?php include "static/html/includes.html" ?>

</head>
<body>

	<?php include "static/html/header.html" ?>

	<main>

		<section id="notebook">
			<div class="note" id="motd">
				<h3>MOTD</h3>
				<?php include "MOTD.html" ?>
			</div>
			<div class="note">
				<?php
					$history = $DB->query("SELECT * FROM history ORDER BY `id` DESC LIMIT 1");
					if (!$history) {
						$DB->query("CALL save_history()");
						$history = $DB->query("SELECT * FROM history ORDER BY `id` DESC LIMIT 1");
					}
					$history = $history[0];
				?>
				<h3>Data</h3>
				<small>from <?php echo $history["timestamp"]." ".date_default_timezone_get()  ?></small>
				<table>
					<thead>
						<tr>
							<th>Key</th>
							<th>Value</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$keys = Array(
								"users" => "Users",
								"anonymous" => "Anonymous",
								"files" => "Files",
								"staticpages" => "Static Pages",
								"shorturis" => "Short URIs"
							);
							foreach ($keys as $key => $nicer) {
								echo "<tr><td>$nicer</td><td>$history[$key]</td></tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</section>

		<section>
			<img src="/img/gnucat.svg" id="gnucat">
			<h3>The GNUCAT Web Coreutils</h3>
			<p>
				Welcome to GNUCAT, this wepsite [sic] tries to bring users a set of online tools, just like the GNU Coreutils and GNU/Linux Operating System brings to people on local, but online.
				<br>
				This website is not related to the GNU Project, although it is the mission of the developer to be included in such.
			</p>
			<p>
				People communicate with other people online, and when people communicate, they sometimes need to exchange more than a quick rundown, maybe an image of your cat, a source code file, or maybe a little song you made. The GNUCAT Coreutils set of tools from this website lets users do this in a really leet and cool manner, not only impressing your friends and future wife, but improving your life by at least 150%.
			</p>
			<p>
				Online there are several tools that already work like the tools offered, but they have two problems, the inconvenience of using multiple web tools for multiple things, and the complete lack of freedom of owning the software you are using.
				<br>
				GNUCAT tries to fight these problems, first of all, GNUCAT is <strong>free as in freedom</strong>, implementing the Affero GNU Public License for its source code, meaning that, when somebody else improves this software, they also share their improvements with the community, plus all the benefits of the GPL.
			</p>
			<p style="clear: left">
				If you want to learn more about GNUCAT, please <a href="https://gitlab.com/gnucat/gnucat/-/blob/master/README">README</a>, or visit the <a href="/contact">CONTACT</a> page for info about its members.
				<br>
				If you want to learn more about the Free Software movement, please visit the <a href="https://www.fsf.org/">Free Software Foundation website</a>.
			</p>
		</section>

	</main>

	<?php include "static/html/footer.html" ?>

</body>
</html>
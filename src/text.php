<?php

	require "init.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title>GNUCAT</title>

	<?php include "static/html/includes.html" ?>

	<script>
		/*
		@licstart  The following is the entire license notice for the 
		JavaScript code in this page.

		Copyright (C) 2021  Jaume Fuster i Claris

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU Affero General Public License as
		published by the Free Software Foundation, either version 3 of the
		License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU Affero General Public License for more details.

		As additional permission under GNU AGPL version 3 section 7, you
		may distribute non-source (e.g., minimized or compacted) forms of
		that code without the copy of the GNU AGPL normally required by
		section 4, provided you include this license notice and a URL
		through which recipients can access the Corresponding Source.   

		@licend  The above is the entire license notice
		for the JavaScript code in this page.
		*/
	</script>

	<script type="text/javascript">
		function parseForm(form_element, event) {
			event.preventDefault()

			let form = new FormData(form_element)
			
			let username = form.get("auth:username")
			let password = form.get("auth:password")
			if (username && password)
				form.set("auth", username+":"+password)

			let text = form.get("text")
			var file = new File([text], "text.txt", {type: "text/plain"})
			form.set("file", file)

			let request = new XMLHttpRequest()
			request.onload = function (e) {
				if (request.response.startsWith("http"))
					window.location = request.response
				else
					alert(request.response)
			}
			request.open("POST", "/file")
			request.send(form)
		}
	</script>

</head>
<body>

	<?php include "static/html/header.html" ?>

	<main>

		<div id="notebook">
			<div class="note">
				<form method="POST" enctype="multipart/form-data" onsubmit="parseForm(this, event)">
					<fieldset>
						<legend>Upload a Text</legend>
						<label>
							<textarea name="text"></textarea>
						</label>
						<input type="submit" value="Upload">
					</fieldset>
				</form>
			</div>
			<div class="note">
				Don't want to code your own script to use gnu.cat?
				<a href="/static/gnucat" download>Download this POSIX compliant tool!</a>
			</div>
		</div>

		<section>
			<h3>Using the GNUCAT script</h3>
			<p>
				There are several ways to upload a text to gnu.cat using the <a href="/static/gnucat" download>POSIX compliant tool</a><br>
				For example, to simply upload the text inside a file, use <code>gnucat -f file.txt</code> or, even more leet: <code>cat coolfile.txt | gnucat -t</code>, <code>-t</code> also has the same optional flags as <code>-f</code> for account, self destruct, etc.
			</p>
			<p>
				In the back end, this is the same as the file upload tool.
			</p>
			<p>
				There is not a lot of information on this page, so I will put this here to fill some space:
			</p>
			<pre>
				 _______________
				< GNUCAT Rules! >
				 ---------------
				        \   ^__^
				         \  (oo)\_______
				            (__)\       )\/\
				                ||----w |
				                ||     ||
			</pre>
		</section>

	</main>

	<?php include "static/html/footer.html" ?>

</body>
</html>
<?php

class DB {

	protected $link;
	
	function __construct($config) {
		$this->link = new mysqli(
			$config["host"],
			$config["username"],
			$config["password"],
			$config["database"]
		);
	}

	function escape($string) {
		return $this->link->real_escape_string($string);
	}

	function log() {
		die($this->link->error);
	}

	function query($sql) {
		$result = $this->link->query($sql);
		if (is_object($result)) {
			$values = Array();
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
				array_push($values, $row);
			return $values;
		} else if ($result) {
			$id = $this->link->insert_id;
			$affected = $this->link->affected_rows;
			return $affected == 1 ? $id : $affected;
		} else return False;
	}

}

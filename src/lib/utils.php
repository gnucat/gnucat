<?php

namespace utils;

function getUser() {
	global $DB;
	$user = "NULL";
	if (isset($_POST["auth"]) and !empty($_POST["auth"])) {
		$auth = explode(":", $_POST["auth"], 2);
		$username = $DB->escape($auth[0]);
		$hashed_password = $DB->query("SELECT pass FROM users WHERE user = '$username'");
		if ($hashed_password) {
			$password = $auth[1];
			$hashed_password = $hashed_password[0]["pass"];
			if (password_verify($password, $hashed_password)) {
				$user = "'$username'";
			}
		}
	}
	return $user;
}

function getSelfDestruct() {
	global $DB;
	$selfdestruct = "NULL";
	if (isset($_POST["selfdestruct"]) and !empty($_POST["selfdestruct"]))
		$selfdestruct = "'".$DB->escape($_POST["selfdestruct"])."'";
	return $selfdestruct;
}

function getPassword() {
	$password = "NULL";
	if (isset($_POST["password"]) and !empty($_POST["password"]))
		$password = "'".password_hash($_POST["password"], PASSWORD_BCRYPT)."'";
	return $password;
}

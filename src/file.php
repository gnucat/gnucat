<?php

	require "init.php";


	if ($_SERVER["REQUEST_METHOD"] === "POST") {

		if (!isset($_FILES["file"]["name"]))
			die("gnu.cat: error: No file uploaded.");

		if ($_FILES["file"]["error"]) {
			// https://www.php.net/manual/en/features.file-upload.errors.php
			$errors = Array(
				1 => "The uploaded file exceeds the maximum size we arbitrarily accept.",
				2 => "The uploaded file exceeds the maximum size we arbitrarily accept.",
				3 => "The upload was cut mid way for some bizarre and scary reason.",
				4 => "I don't even know what this error meant.",
				5 => "Does this error even exist?",
				6 => "We fucked up the server big time.",
				7 => "Maybe the disk on the server is full or something, idk man, call 911.",
				8 => "I don't even know what this error means."
			);
			die("gnu.cat: error: ".$errors[$_FILES["file"]["error"]]);
		}

		$user = \utils\getUser();
		$selfdestruct = \utils\getSelfDestruct();
		$password = \utils\getPassword();
		$filename = $DB->escape($_FILES["file"]["name"]);
		$mimetype = $DB->escape(mime_content_type($_FILES["file"]["tmp_name"]));

		$extension = "";
		if (isset($_POST["extension"]))
			$extension = ".".pathinfo($filename, PATHINFO_EXTENSION);

		$id = $DB->query(
			"INSERT INTO files VALUES 
				(reserve_uri($user, 'files'), '$filename', '$mimetype', $selfdestruct, $password)"
		);
		$uri = $DB->query("SELECT uri FROM uris WHERE id = $id")[0]["uri"];

		move_uploaded_file($_FILES["file"]["tmp_name"], "dynamic/files/".$uri);

		die(PROTOCOL."$_SERVER[HTTP_HOST]/$uri$extension");

	}

?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title>GNUCAT</title>

	<?php include "static/html/includes.html" ?>

		<script>
			/*
			@licstart  The following is the entire license notice for the 
			JavaScript code in this page.

			Copyright (C) 2021  Jaume Fuster i Claris

			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as
			published by the Free Software Foundation, either version 3 of the
			License, or (at your option) any later version.

			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
			GNU Affero General Public License for more details.

			As additional permission under GNU AGPL version 3 section 7, you
			may distribute non-source (e.g., minimized or compacted) forms of
			that code without the copy of the GNU AGPL normally required by
			section 4, provided you include this license notice and a URL
			through which recipients can access the Corresponding Source.   

			@licend  The above is the entire license notice
			for the JavaScript code in this page.
			*/
		</script>

	<script type="text/javascript">
		function parseForm(form_element, event) {
			event.preventDefault()

			let form = new FormData(form_element)
			
			let username = form.get("auth:username")
			let password = form.get("auth:password")
			if (username && password)
				form.set("auth", username+":"+password)

			let request = new XMLHttpRequest()
			request.onload = function (e) {
				if (request.response.startsWith("http"))
					window.location = request.response
				else
					alert(request.response)
			}
			request.open("POST", "/file")
			request.send(form)
		}
	</script>

</head>
<body>

	<?php include "static/html/header.html" ?>

	<main>

		<div id="notebook">
			<div class="note">
				<form method="POST" enctype="multipart/form-data" onsubmit="parseForm(this, event)">
					<fieldset>
						<legend>Upload a File</legend>
						<fieldset>
							<legend>Optional</legend>
							<input type="text" name="auth:username" placeholder="User">
							<input type="password" name="auth:password" placeholder="Password">
							<label>
								Self-destruct on
								<input type="date" name="selfdestruct">
							</label>
							<label>
								<input type="password" name="password" placeholder="File Password">
							</label>
							<label class="checkbox">
								<input type="checkbox" name="extension" onchange="
									this.checked ? 
										this.parentElement.classList.add('checked') :
										this.parentElement.classList.remove('checked')
								">
								Show extension on URI.
							</label>
							<noscript>
								<input type="checkbox" name="extension"> Show extension (for real this time)
							</noscript>
						</fieldset>
						<label class="file">
							<input type="file" name="file" onchange="
								document.getElementById('filename').innerHTML=this.value.match(/[^\\/]*$/)[0]
							">
							Browse...
						</label>
						<span id="filename">
							No file selected.
							<noscript>Or maybe there is?!</noscript>
						</span>
						<br>
						<input type="submit" value="Upload">
					</fieldset>
				</form>
			</div>
			<div class="note">
				Don't want to code your own script to use gnu.cat?
				<a href="/static/gnucat" download>Download this POSIX compliant tool!</a>
			</div>
		</div>

		<section>
			<h3>Using the GNUCAT script</h3>
			<p>
				You have several methods of using GNUCAT, you can code your script, use the web interface at the right
				of this page, or download the <a href="/static/gnucat" download>POSIX compliant tool</a>.
			</p>
			<p>
				You can use <code>gnucat -h</code> to view its usage, but a simple command to upload a file is just 
				<code>gnucat -f <u>filename</u></code> where filename is the file name, duh.
			</p>
			<p>
				To change the instance or, if you have an account, the user and password, edit the script, lines 4, 5
				and 6 with your configuration.
			</p>
			<?php include "static/man/file.html" ?>
		</section>

	</main>

	<?php include "static/html/footer.html" ?>

</body>
</html>
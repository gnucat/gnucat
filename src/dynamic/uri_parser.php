<?php

require "../init.php";


// https://developer.mozilla.org/en-US/docs/Web/Media/Formats
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
const DISPLAYABLE = "/^(image\/|video\/|audio\/|text\/(plain|csv|xml)|application\/json)/";


$uri = explode(".", $_GET["uri"])[0];
$uri = $DB->escape($uri);
$resource = $DB->query("SELECT id, type FROM uris WHERE uri = '$uri'");

if (!$resource) {
	http_response_code(404);
	include "../static/error/404.html";
	die();
} else $resource = $resource[0];

$id = $resource["id"];

switch ($resource["type"]) {

	case "files":
		$file = $DB->query(
			"SELECT filename, mimetype, password FROM files WHERE id = $id"
		)[0];
		// TODO: password stuff
		header("Content-Disposition: filename=$file[filename]");
		header("Content-Type: $file[mimetype]");
		header("Content-Length: ".filesize("files/$uri"));
		if (!preg_match(DISPLAYABLE, $file["mimetype"])) {
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
		}
		$contents = file_get_contents("files/$uri");
		die($contents);

	case "texts":
		$text = $DB->query("SELECT title, content FROM texts WHERE id = $id")[0];
		header("Content-Type: text/plain");
		header("Content-Disposition: filename=$text[title].txt");
		die($text["content"]);
	
	default:
		die("gnu.cat: error: I don't even know what happened!");

}

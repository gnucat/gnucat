<?php

const grid_size = 20;
const padding = 10;
const width = 200;
const height = 40;

$image = imagecreate(width + padding*2, height + padding*2);
imagecolorallocate($image, 255, 255, 255);

$line = imagecolorallocate($image, 255, 200, 200);
$color = imagecolorallocate($image, 255, 0, 0);

$oldy = -1;
$oldx = -1;
$characters = 5;
$alphabet = range("A", "Z");
$seats = Array();
$password = "";

while ($characters-->0) {

	shuffle($alphabet);

	do {
		$x = random_int(0, width/grid_size-1)*grid_size + padding;
		$y = random_int(0, height/grid_size-1)*grid_size + padding;
	} while (in_array([$x, $y], $seats) or $oldx == $x or $oldy == $y);

	array_push($seats, [$x, $y]);

	/*$color = imagecolorallocate(
		$image,
		random_int(0, 200),
		random_int(0, 200),
		random_int(0, 200)
	);*/

	imagechar($image, 4, $x, $y, $alphabet[1], $color);
	$password .= $alphabet[1];

	if ($oldx !== -1) {
		imageline($image, $oldx, $oldy, $x, $y, $line);
	}

	$oldx = $x;
	$oldy = $y;

	$color = imagecolorallocate($image, 0, 0, 0);

}

// Output the image
header('Content-type: image/png');

imagepng($image);
imagedestroy($image);

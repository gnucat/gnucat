
INSERT INTO users VALUES
    (1, 'capuno', '$2y$10$f4B2J/QZhyDO26GudvtL1OC2hfuPvd0IFgJjNV8m2Xh2hIvIoJEUu', 'c@capuno.cat'),
    (2, 'test', '$2y$10$e8dtrgkfMSfZTBrqXkWx4Oq7ALFd0nya5xhw384MJRm9H93QyF3vS', NULL);

INSERT INTO urls VALUES
    (1, 1, 'https://gnu.cat/', 'aaa');

INSERT INTO files VALUES
    (reserve_uri('capuno', 'files'), 'very_cool.jpeg', 'image/png', '2021-12-31 12:00', NULL),
    (reserve_uri(NULL, 'files'), 'wow_amazinga.pdf', 'idk', '2022-01-01 13:37', NULL);

INSERT INTO staticpages VALUES
    (1, 1, 'lolol', NULL);

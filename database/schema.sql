
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    `id` SERIAL PRIMARY KEY,
    `user` VARCHAR(32) NOT NULL UNIQUE,
    -- bcrypt
    `pass` CHAR(60) NOT NULL,
    -- RFC 5321 Section 4.5.3.1, Local-part (64) plus Domain (256 including separators)
    `mail` VARCHAR(320)
);


DROP TABLE IF EXISTS uris;
CREATE TABLE uris (
    `id` SERIAL PRIMARY KEY,
    `user` BIGINT REFERENCES users(`id`) ON DELETE CASCADE,
    `uri` VARCHAR(8) NOT NULL,
    -- This is not MongoDB...
    `type` ENUM('files', 'staticpages') NOT NULL
);


DROP TABLE IF EXISTS urls;
CREATE TABLE urls (
    `id` SERIAL PRIMARY KEY,
    `user` BIGINT REFERENCES users(`id`) ON DELETE CASCADE,
    -- RFC 7230 Section 3.1.1
    `url` VARCHAR(8000) NOT NULL,
    `uri` VARCHAR(8) NOT NULL
);


DROP TABLE IF EXISTS files;
CREATE TABLE files (
    `id` SERIAL PRIMARY KEY REFERENCES uris(`id`) ON DELETE CASCADE,
    `filename` VARCHAR(255),
    -- RFC 6838 Section 4.2
    `mimetype` VARCHAR(64),
    `selfdestruct` DATETIME DEFAULT NULL,
    `password` CHAR(60)
);


DROP TABLE IF EXISTS staticpages;
CREATE TABLE staticpages (
    `id` SERIAL PRIMARY KEY REFERENCES uris(`id`) ON DELETE CASCADE,
    `selfdestruct` DATETIME DEFAULT NULL
);


DROP TABLE IF EXISTS history;
CREATE TABLE history (
    `id` SERIAL PRIMARY KEY,
    `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `users` INT NOT NULL DEFAULT 0,
    `anonymous` INT NOT NULL DEFAULT 0,
    `files` INT NOT NULL DEFAULT 0,
    `staticpages` INT NOT NULL DEFAULT 0,
    `shorturis` INT NOT NULL DEFAULT 0
);


-- FUNCTIONS AND PROCEDURES

SET GLOBAL log_bin_trust_function_creators = 1; -- cringe
DROP FUNCTION IF EXISTS reserve_uri;
DELIMITER $$
CREATE FUNCTION reserve_uri (user_ VARCHAR(32), type_ VARCHAR(16)) RETURNS BIGINT
MODIFIES SQL DATA
BEGIN

    SET @uri_length = 5;
    SET @user = NULL;
    SET @uri = '';

    IF (user_ IS NOT NULL) AND (EXISTS(SELECT id FROM users WHERE user = user_)) THEN
        SET @uri_length = 3;
        SET @user = (SELECT id FROM users WHERE user = user_);
    END IF;

    WHILE LENGTH(@uri) < @uri_length DO
        SET @uri = CONCAT(@uri, CHAR(CEILING(RAND()*26)+96));
        IF (LENGTH(@uri) = @uri_length) AND 
          ((EXISTS(SELECT uri FROM uris WHERE uri = @uri)) OR @uri IN ('file', 'contact', 'data')) THEN
            SET @uri = '';
        END IF;
    END WHILE;

    INSERT INTO uris (user, uri, type) VALUES (@user, @uri, type_);
    RETURN LAST_INSERT_ID();
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS save_history;
DELIMITER $$
CREATE PROCEDURE save_history ()
BEGIN

    SET @users = (SELECT COUNT(*) FROM users);
    SET @anonymous = (SELECT COUNT(*) 
                        FROM (SELECT user FROM urls
                                    UNION ALL
                                SELECT user FROM uris
                                    UNION ALL
                                SELECT user FROM staticpages
                            ) as content
                        WHERE user IS NULL);
    SET @files = (SELECT COUNT(*) FROM files);
    SET @staticpages = (SELECT COUNT(*) FROM staticpages);
    SET @shorturis = (SELECT COUNT(*) FROM urls);

    INSERT INTO history (users, anonymous, files, staticpages, shorturis)
        VALUES (@users, @anonymous, @files, @staticpages, @shorturis);

END$$
DELIMITER ;

<?php

require "src/init.php";

/* cringest moment in human history
const NEW_HISTORY = "INSERT INTO history 
                        (users, anonymous, files, texts, staticpages, shorturis)
                      SELECT 
                          SUM(CASE WHEN users.id = users.id THEN 1 ELSE 0 END) AS users,
                          SUM(CASE WHEN content.user IS NULL THEN 1 ELSE 0 END) AS anonymous,
                          SUM(CASE WHEN files.id = files.id THEN 1 ELSE 0 END) AS files,
                          SUM(CASE WHEN texts.id = texts.ID THEN 1 ELSE 0 END) AS texts,
                          SUM(CASE WHEN staticpages.id = staticpages.ID THEN 1 ELSE 0 END) AS staticpages,
                          SUM(CASE WHEN uris.id = uris.ID THEN 1 ELSE 0 END) AS shorturis
                        FROM
                          users,
                          (
                            SELECT user FROM urls
                              UNION ALL
                            SELECT user FROM uris
                              UNION ALL
                            SELECT user FROM staticpages
                          ) as content,
                          files,
                          texts,
                          staticpages,
                          uris
                        FULL OUTER J"; // Holy shit this query was long
*/


$last_history = $DB->query("SELECT `timestamp` FROM history ORDER BY `id` DESC LIMIT 1");

const ONE_DAY = 60*60*24;
if (!$last_history or time()-strtotime($last_history[0]["timestamp"]) > ONE_DAY) {
	$DB->query("CALL save_history()");
}
